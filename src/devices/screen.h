/*
Copyright (c) 2021 Devine Lu Linvega
Copyright (c) 2021 Andrew Alderwick
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define FIXED_SIZE 0

typedef struct Layer {
	Uint8 *pixels, changed;
} Layer;

typedef struct TileLayer {
	Uint16 tileAddr, mapAddr, scrollX, scrollY;
	Uint8 width, height, crop, attrMap, twobpp, blend, priority;
} TileLayer;

typedef struct UxnScreen {
	Uint32 palette[4], *pixels;
	Uint16 width, height;
	Layer fg, bg;
	Uint8 mono;

	/* TL Additions */
	TileLayer tile[2];
	Uint16 winEdge[4];
} UxnScreen;

extern UxnScreen uxn_screen;

void screen_palette(UxnScreen *p, Uint8 *addr);
void screen_resize(UxnScreen *p, Uint16 width, Uint16 height);
void screen_clear(UxnScreen *p, Layer *layer);
void screen_redraw(UxnScreen *p, Uxn *u);
void screen_mono(UxnScreen *p, Uxn *u);

Uint8 screen_dei(Uxn *u, Uint8 addr);
void screen_deo(Uint8 *ram, Uint8 *d, Uint8 port);
int clamp(int val, int min, int max);
