#include <stdlib.h>
#include <stdio.h>

#include "../uxn.h"
#include "screen.h"
#include "raster.h"

/*
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

/* IO */

void
raster_deo(Uint8 *ram, Uint8 *d, Uint8 port)
{
	switch(port) {
    case 0x4:
        Uint16 x = PEEK2(d - 0xe0 + 0x28), y = PEEK2(d - 0xe0 + 0x2a);
        if(d[0x4] & 0x01) uxn_screen.tile[0].scrollX = x;
        if(d[0x4] & 0x02) uxn_screen.tile[0].scrollY = y;
        if(d[0x4] & 0x04) uxn_screen.tile[1].scrollX = x;
        if(d[0x4] & 0x08) uxn_screen.tile[1].scrollY = y;
        if(d[0x4] & 0x10) uxn_screen.winEdge[0] = x;
        if(d[0x4] & 0x20) uxn_screen.winEdge[1] = y;
        if(d[0x4] & 0x40) uxn_screen.winEdge[2] = x;
        if(d[0x4] & 0x80) uxn_screen.winEdge[3] = y;
        break;
	case 0x5:
        if(d[0x5] & 0x02) {
            uxn_screen.tile[d[0x5] & 0x1].mapAddr = PEEK2(d - 0xe0 + 0x2c);
        }
        uxn_screen.tile[d[0x5] & 0x1].width = ((d[0x5] & 0x30) + 16);
        uxn_screen.tile[d[0x5] & 0x1].height = ((d[0x5] & 0xc0) + 64) >> 2;
		break;
	case 0x6:
        if(d[0x6] & 0x02) {
            uxn_screen.tile[d[0x6] & 0x1].tileAddr = PEEK2(d - 0xe0 + 0x2c);
        }
        uxn_screen.tile[d[0x6] & 0x1].twobpp = !!(d[0x6] & 0x4);
        uxn_screen.tile[d[0x6] & 0x1].priority = !!(d[0x6] & 0x8);
        uxn_screen.tile[d[0x6] & 0x1].blend = (d[0x6] & 0xf0) >> 4;
		break;
	}
}
